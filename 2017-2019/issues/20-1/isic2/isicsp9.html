﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Windows version 5.2.0">
    <title>
      Uncertainty in reference and information service
    </title>
    <meta charset="utf-8">
    <link href="../../IRstyle3.css" rel="stylesheet" media="screen" title="serif">
    <link rel="alternate stylesheet" type="text/css" media="screen" title="sans" href="../../IRstylesans.css"><!--Enter appropriate data in the content fields-->
    <meta name="dcterms.title" content="Uncertainty in reference and information service">
    <meta name="author" content="Amy VanScoy">
    <meta name="dcterms.subject" content="This paper discusses the theme 'uncertainty and variety' which resulted from an IPA study and proposes a model of uncertainty in reference and information service.">
    <meta name="description" content="Uncertainty is understood as an important component of the information seeking process, but it has not been explored as a component of reference and information service. Interpretative phenomenological analysis was used to examine the practitioner perspective of reference and information service for eight academic research librarians in the United States. Data were analyzed thematically according to interpretative phenomenological analysis procedures. Variety and uncertainty emerged as one of the themes of experience for these practitioners. Based on the results of this study, a conceptual model is proposed for uncertainty in reference and information service. The conceptual model of uncertainty in reference and information service can be used for further study of the phenomenon and to contribute to a comparative sociology of uncertainty across professions. In addition, it can be used to support practitioners and to better prepare students for the uncertainties of practice.">
    <meta name="keywords" content="reference and information service, qualitative research methods, uncertainty"><!--leave the following to be completed by the Editor-->
    <meta name="robots" content="all">
    <meta name="dcterms.publisher" content="Professor T.D. Wilson">
    <meta name="dcterms.type" content="text">
    <meta name="dcterms.identifier" content="ISSN-1368-1613">
    <meta name="dcterms.identifier" content="http://InformationR.net/ir/20-1/isic2/isic...">
    <meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/20-1/infres201.html">
    <meta name="dcterms.format" content="text/html">
    <meta name="dc.language" content="en">
    <meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">
    <meta name="dcterms.issued" content="2015-03-15">
    <meta name="geo.placename" content="global">
  </head>
  <body>
    <header>
      <h4>
        vol. 20 no. 1, March, 2015
      </h4>
    </header>
    <article>
      <h1>
        Uncertainty in reference and information service
      </h1><br>
      <div style="margin-left: 5%; margin-right:10%;">
        <h4>
          <a href="#author">Amy VanScoy</a><br>
          Department of Library &amp; Information Studies, University at Buffalo
        </h4>
      </div><br>
      <form>
        <fieldset>
          <legend>Abstract</legend>
          <blockquote>
            <strong>Introduction.</strong> Uncertainty is understood as an important component of the information seeking process, but it has not been explored as a component of reference and information service.<br>
            <strong>Method.</strong> Interpretative phenomenological analysis was used to examine the practitioner perspective of reference and information service for eight academic research librarians in the United States.<br>
            <strong>Analysis.</strong> Data were analyzed thematically according to interpretative phenomenological analysis procedures.<br>
            <strong>Results.</strong> <em>Variety and uncertainty</em> emerged as one of the themes of experience for these practitioners. Based on the results of this study, a conceptual model is proposed for uncertainty in reference and information service.<br>
            <strong>Conclusion.</strong> The conceptual model of uncertainty in reference and information service can be used for further study of the phenomenon and to contribute to a comparative sociology of uncertainty across professions. In addition, it can be used to support practitioners and to better prepare students for the uncertainties of practice.
          </blockquote>
        </fieldset>
      </form><br>
      <br>
      <section>
        <h2>
          Introduction
        </h2>
        <p>
          Uncertainty is understood as an important component of the information seeking process (for example, Kuhlthau, <a href="#kuh93">1993</a>, <a href="#kuh03">2003</a>) and has been studied as an issue in information retrieval and systems design (for example, <a href="#bra13">Brashers and Hogan, 2013</a>). However, it has not been explored as a component of reference and information service.
        </p>
        <p>
          In a recent study of the practitioner perspective, uncertainty, ambiguity and variability emerged as a major theme in the experience of reference and information service for practitioners. This theme has also been found in research on professional work for other client-centered professions, such as medicine and social work. This short paper explores this theme in relation to the existing literature from other professions and proposes future work to more fully understand the concept of uncertainty in reference and information service.
        </p>
        <h2>
          Related research
        </h2>
        <p>
          The most relevant previous research in this area is Bouthillier's (<a href="#bou00">2000</a>) study of ambiguity for public library service providers. In this ethnography, Bouthillier identified ambiguities at various levels: '<em>ideological, structural and operational</em>' (p. 260), and argued that these ambiguities must be incorporated into conceptualizations of service.
        </p>
        <p>
          Beyond Bouthillier's study, however, uncertainty has not been explored in reference and information service. It has, however, been explored extensively outside library and information science. Schön (<a href="#sch83">1983</a>) describes professional work as occurring in ambiguous situations where problems are not clear or easily understood. Medicine is the profession that has most thoroughly studied uncertainty in professional work. Some notable research includes Fox's (<a href="#fox57">1957</a>) study of areas of uncertainty in the training of medical professionals; Light's (<a href="#lig79">1979</a>) discussion of uncertainty in psychiatry; Gerrity, Earp and DeVellis' (<a href="#ger92">1992</a>) development of an instrument to measure affective response to uncertainty for physicians; and Cranley 's (<a href="#cra12">2012</a>) model of uncertainty in nursing.
        </p>
        <h2>
          Uncertainty in the experience of reference and information service
        </h2>
        <p>
          In a recent study of the practitioner perspective of reference and information service, uncertainty emerged as a theme in the experience of reference and information service. This exploratory study used interpretative phenomenological analysis to study the phenomenon of reference and information service from the practitioner perspective. The aim of the study was to complement existing behavioral studies by examining the thoughts and feelings that motivate behaviors in reference and information service.
        </p>
        <p>
          Interpretative phenomenological analysis was chosen as an approach for examining the experience of reference and information service, as it examines both commonality and diversity of experience, combining aspects of phenomenology and phenomenography (<a href="#van15">VanScoy and Evenstad, in press</a>). The eight participants in this study were academic research librarians working in the United States. The semi-structured interviews focused on the experience of reference and information service for the participants. Interview data was analyzed thematically following interpretative phenomenological analysis procedures. Each participant's data was analyzed individually, then master themes for the group were developed. Five themes of the experience of reference and information service emerged from the analysis: <em>importance of the user</em>, <em>variety and uncertainty</em>, <em>fully engaged practice</em>, <em>emotional connection</em>, and <em>sense of self as reference professional</em> (<a href="#van13">VanScoy, 2013</a>).
        </p>
        <p>
          The theme variety and uncertainty was an important theme for these participants. Variety was expressed explicitly with phrases such as '<em>a lot of variety</em>','<em>change of pace and variety at all levels</em>'and '<em>it's a mixed bag</em>'. Coupled with variety was a sense of uncertainty or ambiguity. Participants stated that they never knew what questions they might be asked, what path a reference interaction might take, or what reaction a user might give, using phrases such as '<em>you never know what you're gonna get or where it goes</em>', '<em>when you work at a reference desk, it's like anyone can ask you any conceivable question at any time</em>', and '<em>[you] really, truly never know what's gonna come at you'</em>. Other phrases hint at a lack of control for the direction of the interaction: '<em>wherever it takes you</em>' and '<em>you're not in control of what's going on'</em>.
        </p>
        <p>
          The variety of users and user needs, along with the variety of locations and modes of delivery, demands nimbleness and diverse skills, In addition, the variety of purposes of the interaction, coupled with the fact that the ultimate purpose for any given interaction is not known at the outset, creates even more uncertainty for the librarian to cope with.
        </p>
        <p>
          This finding supports Bouthillier's (<a href="#bou00">2000</a>) findings about ambiguity for library service providers. It also supports a similar finding by Burns and Bossaller (<a href="#bur12">2012</a>), who studied communication in reference and information service. Although they did not find uncertainty as a theme, they were surprised by the complexity of the work environment that their participants described: '<em>a confusing and unstable landscape</em>' (p. 613) featuring '<em>an experience much more varied and intricate than imagined before the study</em>' (p. 612).
        </p>
        <h2>
          Future directions for uncertainty in reference and information service
        </h2>
        <p>
          These findings support the claim that the experience of reference and information service is uncertain, varied and ambiguous. However, the nature of this uncertainty and how it affects practice is unclear.
        </p>
        <p>
          A first step toward understanding this uncertainty is development of a conceptual model of uncertainty in reference and information service. Based on the results of the interpretative phenomenological analysis study of the experience of reference and information service for academic research librarians, Bouthillier's study of ambiguity for public library service providers, and models developed for physicians (<a href="#ger92">Gerrity, Earp and DeVellis, 1992</a>) and nurses (<a href="#cra12">Cranley , 2012</a>), a model is proposed that identifies areas of uncertainty in reference and information service (See Figure 1). Through the center of the model are the stages of the reference interaction where uncertainty may occur. Contributing to uncertainty in the interaction are user, professional and organizational characteristics. This model may be used as tool for further study, refining the elements presented and examining relationships between them.
        </p>
        <figure class="centre">
          <img src="isicsp9fig1.jpg" alt="Figure1: A model for uncertainty in reference and information service" width="600" height="475">
          <figcaption>
            Figure 1: A model for uncertainty in reference and information service
          </figcaption>
        </figure>
        <p>
          Understanding uncertainty in reference and information service could contribute to the larger dialogue of professional uncertainty, responding to Gerrity, Earp and DeVellis' (<a href="#ger92">1992</a>) call for a '<em>comparative sociology of uncertainty</em>' (p. 1044). On a more practical level, articulation of the uncertainty involved in reference and information service would provide support to practitioners and help avoid the temptation to over control uncertain situations by ignoring complexity and refusing to question one's decisions. Library and information science educators could use such a model to prepare students for the uncertainties of practice.
        </p>
        <h2 id="author">
          About the author
        </h2>
        <p>
          <strong>Amy VanScoy</strong> is an Assistant Professor in the Department of Library and Information Studies, University at Buffalo, Buffalo, NY. She received her PhD from the University of North Carolina at Chapel Hill and her MLIS from the University of Alabama. She can be contacted at: vanscoy@buffalo.edu.
        </p>
      </section>
      <section class="refs">
        <form>
          <fieldset>
            <legend style="color: white; background-color: #5E96FD; font-size: medium; padding: .1ex .5ex; border-right: 1px solid navy; border-bottom: 1px solid navy; font-weight: bold;">References</legend>
            <ul class="refs">
              <li id="bou00">Bouthillier, F. (2000). The meaning of service: ambiguities and dilemmas for public library service providers. <em>Library &amp; Information Science Research</em>, 22(3), 243-272.
              </li>
              <li id="bra13">Brashers, D. E. &amp; Hogan, T. P. (2013). The appraisal and management of uncertainty: implications for information-retrieval systems. <em>Information Processing &amp; Management</em>, 49(6), 1241-1249.
              </li>
              <li id="bur12">Burns, S. &amp; Bossaller, J. (2012). Communication overload: a phenomenological inquiry into academic reference librarianship. <em>Journal of Documentation</em>, 68(5), 597-617.
              </li>
              <li id="cra12">Cranley, L.A., Doran, D.M., Tourangeau, A.E, Kushniruk, A. &amp; Nagle, L. (2012). Recognizing and responding to uncertainty: a grounded theory of nurses' uncertainty. <em>Worldviews on Evidence-Based Nursing</em> 9(3), 149-158.
              </li>
              <li id="fox57">Fox, R. C. (1957). Training for uncertainty. In R.K. Merton, G. Reader &amp; P.L. Kendall (Eds.), <em>The student physician: introductory studies in the sociology of medical education</em> (pp. 207-241). Cambridge, MA: Harvard University Press.
              </li>
              <li id="ger92">Gerrity, M.S., Earp, J.A., DeVellis, R.F. &amp; Light, D.W. (1992). Uncertainty and professional work: perceptions of physicians in clinical practice. <em>American Journal of Sociology</em>, 97(4), 1022-1051.
              </li>
              <li id="kuh93">Kuhlthau, C. C. (1993). A principle of uncertainty for information seeking. <em>Journal of Documentation</em>, 49(4), 339-355.
              </li>
              <li id="kuh03">Kuhlthau, C. C. (2003). <em>Seeking meaning: a process approach to library and information services.</em> Englewood, CO: Libraries Unlimited.
              </li>
              <li id="lig79">Light, D. (1979). Uncertainty and control in professional training. <em>Journal of Health and Social Behavior</em>, 20, 310-322.
              </li>
              <li id="sch83">Schön, D. A. (1983). <em>The reflective practitioner : how professionals think in action</em>. New York: Basic Books.
              </li>
              <li id="van13">VanScoy, A. (2013). Fully engaged practice and emotional connection: aspects of the practitioner perspective of reference and information service. <em>Library &amp; Information Science Research</em>, 35(4), 272-278.
              </li>
              <li id="van15">VanScoy, A. &amp; Evenstad, S.B. (in press). Interpretative phenomenological analysis for LIS research. <em>Journal of Documentation</em>.
              </li>
            </ul>
          </fieldset>
        </form>
      </section>
    </article><br>
    <section>
      
      <br>
      <div style="text-align:center;">
        
        
      </div>
      <hr>
      
    </section>
    <footer>
      <hr>
      
      <hr>
    </footer>
  </body>
</html>
