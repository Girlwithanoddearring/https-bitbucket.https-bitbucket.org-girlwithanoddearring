﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta name="generator" content="HTML Tidy for HTML5 for Windows version 5.2.0">
    <title>
      An exploration of community-based organizations' information management challenges
    </title>
    <meta charset="utf-8">
    <link href="../../IRstyle3.css" rel="stylesheet" media="screen" title="serif">
    <link rel="alternate stylesheet" type="text/css" media="screen" title="sans" href="../../IRstylesans.css"><!--Enter appropriate data in the content fields-->
    <meta name="dcterms.title" content="An exploration of community-based organizations' information management challenges">
    <meta name="author" content="tomwilson">
    <meta name="dcterms.subject" content="Give a brief description of your paper">
    <meta name="description" content="Copy your abstract here - remove the structured headings">
    <meta name="keywords" content="Provide keywords, using those in the journal's list (http://informationr.net/ir/Index_terms.html) where possible"><!--leave the following to be completed by the Editor-->
    <meta name="robots" content="all">
    <meta name="dcterms.publisher" content="Professor T.D. Wilson">
    <meta name="dcterms.type" content="text">
    <meta name="dcterms.identifier" content="ISSN-1368-1613">
    <meta name="dcterms.identifier" content="http://InformationR.net/ir/20-1/isic2/isicsp15.html">
    <meta name="dcterms.IsPartOf" content="http://InformationR.net/ir/20-1/isic2/isic2.html">
    <meta name="dcterms.format" content="text/html">
    <meta name="dc.language" content="en">
    <meta name="dcterms.rights" content="http://creativecommons.org/licenses/by-nd-nc/1.0/">
    <meta name="dcterms.issued" content="2015.03.15">
    <meta name="geo.placename" content="global">
  </head>
  <body>
    <header>
      <h4>
        vol. 20 no. 1, March, 2015
      </h4>
    </header>
    <article>
      <h1>
        An exploration of community-based organizations' information management challenges
      </h1><br>
      <div style="margin-left: 5%; margin-right:10%;">
        <h4>
          <a href="#author">Pauline Joseph</a><br>
          Department of Information Studies, School of Media Culture and Creative Arts, Curtin University, GPO Box U 1987, Perth, Western Australia 6845
        </h4>
      </div><br>
      <section>
        <form>
          <fieldset>
            <legend>Abstract</legend>
            <blockquote>
              <strong>Introduction</strong>. Community-based organizations are struggling to manage their current, historical and archival information assets, which impact on the information practices and seeking behaviour in these communities. This paper explores what these challenges are and proposes sustainable solutions with the help of cultural institutions.<br>
              <strong>Methods</strong>, Research data were gathered from student projects, participant workshops and a case study with the motor sport community.<br>
              <strong>Analysis</strong>. This study draws on the qualitative analysis of a range of sources.<br>
              <strong>Results</strong>. The findings scope the information management challenges experienced generally in community-based organisations with detailed insights into the difficulties evident in the motor sport community.<br>
              <strong>Conclusion.</strong> A common system to register, capture, access and preserve community-based information assets is recommended to be hosted by an Australian cultural institution in their capacity as ‘keepers’ of Australia’s history, archives and heritage.
            </blockquote>
          </fieldset>
        </form><br>
        <br>
        <h2>
          Introduction
        </h2>
        <p>
          Community-based organizations promote leisure, social and cultural interests. However, funding and other issues obstruct their ability to manage their information assets. Consequently, it flags the demise of the nation’s community-based achievements and history. This paper explores the key information management challenges community-based organizations experience and how they can be sustainably assisted by cultural institutions.
        </p>
        <h2>
          Research problem
        </h2>
        <p>
          This research was stimulated by a number of years’ exposure working with not-for-profit community-based organizations and a request for assistance received by a motor sport enthusiast.
        </p>
        <p>
          It was evident that these groups had difficulties in capturing, managing and accessing their assets to address their evolving information needs. This led to the identification of the research question:
        </p>
        <p>
          <em>What are the information management challenges of community-based organizations?</em>
        </p>
        <p>
          A secondary question that was then identified was:
        </p>
        <p>
          <em>How can community-based organizations be sustainably assisted in managing their information assets?</em>
        </p>
        <p>
          It was decided to use the motor sport community as a case study to explore answers to the above research questions.
        </p>
        <h2>
          Literature review
        </h2>
        <p>
          A literature review was first undertaken to find out how community-based information management practices are recognised and assisted by national or regional agencies. An analysis of the literature revealed a number of projects within and beyond Australia that advocate sustainable community-based information management practices. Allied to this focus was an interest in exploring the challenges experienced by community-based organizations in harnessing their information assets to address their ongoing information needs. These two areas are explored in this first part of the paper.
        </p>
        <h3>
          Sustainable community-based solutions to information management practices
        </h3>
        <p>
          The state of Victoria in Australia has recognised the need for sustainable community-based information management practices and has developed two common systems: Collectish and Victorian Collections (<a href="#refs">Museum Victoria, 2013</a>). Both are free systems, Collectish is a developed for private collectors to catalogue and showcase their collection, whilst Victorian Collections is intended for collecting organizations (<a href="#refs">Museum Victoria, 2013</a>).
        </p>
        <p>
          The limitation of the Victorian Collections system is that it provides a solution to address community-based collections but only in Victoria. In contrast, The Community Archive is New Zealand’s archival hub for organizations to manage and showcase their collections (<a href="#refs">Archives New Zealand. Department of Internal Affairs, 2014</a>). Outside the Pacific Rim is the Europeana Project that is led by the European Commission. It is a joint initiative by Europe’s museums, archives and libraries to digitise Europe’s cultural heritage and make it accessible online (<a href="#refs">Poole, 2010</a>; <a href="#refs">Purday, 2009, 2012</a>). The Europeana Project aimed to trigger a ‘digital renaissance’ in Europe by enabling “high-quality content on the net for many generations” (<a href="#refs">Purday, 2012</a>, p. 2).
        </p>
        <p>
          In conclusion, The Community Archive system is of interest to this research as it is an example of the type of single and central community-based system solution this research aims to identify for community-based organizations in Australia. In the absence of such a common information infrastructure for community-based organizations, their information practices are either neglected or non-compliant to international best practice records and archive management standards (<a href="#refs">ICA Committee of Best Practices and Standards, 2008</a>; <a href="#refs">International Organisation for Standardisation, 2001a, 2001b, 2011a, 2011b, 2012</a>; <a href="#refs">Sub-Committee on Descriptive Standards, 1999</a>). The absence of guidelines and a national infrastructure creates a major impediment for these groups to capture and manage their information assets. A first step in providing this support is to clarify the types of difficulties community-based organizations may encounter in managing their information assets.
        </p>
        <p>
          The next step is to understand the information seeking behaviour of community-based organizations, so that suitable information management systems are designed to address the community’s information needs.
        </p>
        <h3>
          Information seeking behaviour
        </h3>
        <p>
          This research has amalgamated the definitions of information seeking behaviour in the literature to define it as how and why communities need, seek, create, share, search, use and manage information for their particular niche interests (<a href="#refs">Fisher, Erdelez, and McKechnie, 2005</a>; <a href="#refs">Pettigrew and McKechnie, 2001</a>).
        </p>
        <p>
          There are many theories and models of information seeking behaviour but there is no theory or model on how people in community–based organizations seek information about their specialist activities. This information is often based within their own group’s archives or sources, but they may be difficult to access given their haphazard, inaccessible or even, invisible nature. There is a gap in the literature about: what information they generate and need, what sources they consult or rely upon to fulfil their information need, how they manage their club, organization or personal information and whether they are satisfied with how their information needs are fulfilled.
        </p>
        <p>
          Savolainen’s (<a href="#refs">1995</a>) everyday life information seeking behaviour and Skov’s (<a href="#refs">2009, 2013</a>) hobby-related information seeking behaviour research do provide relevant insights to researching about the information seeking behaviour of community-based organizations. Their research integrates consideration of the models and theories from information science relating to general information seeking behaviour, including Wilson’s (<a href="#refs">2005</a>) information behaviour model and Krikelas’s (<a href="#refs">1983</a>) theory of information seeking behaviour.
        </p>
        <p>
          From this preliminary review it is clear that these groups, which number in the thousands to tens of thousands, fill an important role in society, but are largely unrecognised when we consider information management and the deployment of information to address ongoing information needs. The balance of this paper offers a preliminary insight into these two challenges, drawing on some collective insights from different groups, and a preliminary exploration of one specific specialist group: motor sport clubs.
        </p>
        <h2>
          Research methods
        </h2>
        <p>
          Following the literature review, it was decided to conduct an exploratory scoping study on this topic using two qualitative research methods (<a href="#refs">Denzin and Lincoln, 2005</a>; <a href="#refs">Gorman and Clayton, 2005</a>). The first was by environmental scanning and observations of student projects with community-based organization and workshops I delivered to custodians and representatives of community-based organizations. The second is from a detailed case study, drawing on extensive interviews and discussion with a motor sport enthusiast who sought assistance to address his community’s information management difficulties (<a href="#refs">Merriam, 2009</a>; <a href="#refs">Yin, 2009</a>). In summary, the case study with the motor sport community was used to confirm initial observations with the student projects and workshop participants.
        </p>
        <h2>
          Results
        </h2>
        <p>
          In reviewing the various experiences of the community-based organisations scanned, it became evident that they experienced these key challenges:
        </p>
        <ul>
          <li>difficulty in sourcing, organising and managing their (sometimes dispersed or lost) records and archives;
          </li>
          <li>challenges in accessing past information or precedents to guide future practice;
          </li>
          <li>divergent information management practices within the same community-based organisations, due to different people assuming the role in an unregulated context;
          </li>
          <li>a lack of guidelines or principles to follow;
          </li>
          <li>roles being assumed by unskilled members of the group;
          </li>
          <li>an absence of a national service that would assist in guiding good practice; and
          </li>
          <li>lack of awareness about information management professional standards (International Organisation for Standardisation, 2001a, 2001b, 2012) , guidelines and assistance available from cultural institutions.
          </li>
        </ul>
        <p>
          The motor sport enthusiast’s description of the status of the motor sport community’s information management practices and archives in general and not limited to the historic aspect of motor sport were as follows.
        </p>
        <ul>
          <li>Motor sport archives are unidentified, disorganised and therefore difficult to access by the motor sport community.
          </li>
          <li>The contents are predominantly paper based, thereby restricting access to knowledge and information.
          </li>
          <li>Historic information is scattered and held by various sporting car clubs /associations and at the homes of motoring enthusiasts.
          </li>
          <li>Historic contents range from documents, photographs, websites, memorabilia (trophies, medals, etc.) and historic cars.
          </li>
          <li>Oral or video recordings of prominent personalities and racing film footage have no permanent home. (The Confederation of Australian Motor Sport Limited, 2014 ; T. Benson, personal communication, May 2, 2013)
          </li>
        </ul>
        <h2>
          Conclusion
        </h2>
        <p>
          It is apparent from this preliminary overview to provide community-based organisations with assistance in mapping basic practices and principles that might be followed to commence the process of better managing current information assets and ultimately, collecting, digitising and optimising their past information assets. It highlights the need for Australia to take a larger view of community-based organisations’ asset management.
        </p>
        <p>
          A common system needs to be hosted by an Australian cultural institution like the national or state library, archive or museum, in their capacity as ‘keepers’ of Australia’s history and archive. This is the model used by The Community Archive that is maintained by Archives New Zealand. Ownership by cultural institutions is more likely to provide trust and credibility for communities to register and upload their assets online. Additionally, there is a greater chance to ensure the perpetuity of these cultural archives when in the custody of such institutions than in the hands of volunteer reliant community-based organisations.
        </p>
        <p>
          Further research with the motor sport community as a case study is underway: to explore this community’s information management practices; to identify its significant information assets; individual enthusiast’s information needs and behaviour; their reaction to a common system to manage their collections; and how willing are Australian cultural institutions in developing sustainable common systems. These findings will be published in upcoming papers.
        </p>
      </section>
      <section class="refs">
        <form>
          <fieldset>
            <legend style="color: white; background-color: #5E96FD; font-size: medium; padding: .1ex .5ex; border-right: 1px solid navy; border-bottom: 1px solid navy; font-weight: bold;">References</legend>
            <ul class="refs" id="refs">
              <li>Archives New Zealand. Department of Internal Affairs. (2014). <em><a href="http://thecommunityarchive.org.nz/about_the_community_archive">About the community archive</a></em> Retrieved 17 March, 2014, from http://thecommunityarchive.org.nz/about_the_community_archive
              </li>
              <li>Denzin, N. K., &amp; Lincoln, Y. S. (Eds.). (2005). <em>The Sage handbook of qualitative research</em> (3rd Ed. ed.). London: Sage Publications, Inc.
              </li>
              <li>Fisher, K. E., Erdelez, S., &amp; McKechnie, L. E. F. (Eds.). (2005). <em>Theories of information behavior</em>. New Jersey: Information Today Inc.
              </li>
              <li>Gorman, G., &amp; Clayton, P. (2005). <em>Qualitative research for the information professional: A practical handbook</em> (2nd ed.). London: Facet Publishing.
              </li>
              <li>ICA Committee of Best Practices and Standards. (2008). <em>ICA-ISDIAH: International standard for describing institutions with archival holdings</em> (1st ed.): International Council of Archives.
              </li>
              <li>International Organisation for Standardisation. (2001a). <em>ISO 15489-1: Information and documentation - records management - Part 1 - general</em>. Geneva: International Organisation for Standardisation.
              </li>
              <li>International Organisation for Standardisation. (2001b). <em>ISO 15489-2: Information and documentation - records management - Part 2 - guidelines</em>. Geneva: International Organisation for Standardisation.
              </li>
              <li>International Organisation for Standardisation. (2011a). <em>ISO 30300: Information and documentation, management systems for records - fundamentals and vocabular</em>y. Geneva: International Organisation for Standardisation.
              </li>
              <li>International Organisation for Standardisation. (2011b). <em>ISO 30301: Information and documentation, management systems for records - requirements</em>. Geneva: International Organisation for Standardisation.
              </li>
              <li>International Organisation for Standardisation. (2012). <em>ISO 13028: Information and documentation – implementation guidelines for digitisation of records.</em> Geneva: International Organisation for Standardisation.
              </li>
              <li>Krikelas, J. (1983). Information-seeking behavior: Patterns and concepts. <em>Drexel Library Quarterly, 19,</em>(1), 5-20.
              </li>
              <li>Merriam, S. B. (2009). <em>A guide to design and implementation</em>. New York, NY: John Wiley &amp; Sons, Inc.
              </li>
              <li>Museum Victoria. (2013). <em><a href="http://collectish.com/">Home page of Collectish</a></em> Retrieved April, 2013, from http://collectish.com/
              </li>
              <li>Pettigrew, K., &amp; McKechnie, L. (2001). The use of theory in information science reseach. <em>Journal of the American Society for Science &amp; Technology, 52</em>(1), 62-73.
              </li>
              <li>Poole, N. (2010). <em><a href="http://ec.europa.eu/information_society/activities/digital_libraries/doc/refgroup/annexes/digiti_report.pdf">The cost of digitising Europe’s cultural heritage: A report for the comité des sages of the european commission</a></em> Retrieved June, 2013, from http://ec.europa.eu/information_society/activities/digital_libraries/doc/refgroup/annexes/digiti_report.pdf
              </li>
              <li>Purday, J. (2009). Think culture: Europeana.eu from concept to construction. <em>The Electronic Library, 27</em>(6), 919-937.
              </li>
              <li>Purday, J. (2012). Europeana: Digital access to europe's cultural heritage. <em>Alexandria, 23</em>(2).
              </li>
              <li>Savolainen, R. (1995). <a href="http://informationr.net/ir/17-2/paper516.html">Everyday life information seeking: Approaching information seeking in the context of "way of life"</a>. <em>Library &amp; Information Science Research, 17</em>(3), 259 - 294. Retrieved from http://informationr.net/ir/17-2/paper516.html
              </li>
              <li>Skov, M. (2009). <em>The reinvented museum: Exploring information seeking behaviour in a digital museum context</em>. Unpublished doctoral dissertation. Royal School of Library and Information Science, Copenhagen, Denmark.
              </li>
              <li>Skov, M. (2013). <a href="http://www.informationr.net/ir/18-4/paper597.html">Hobby-related information-seeking behaviour of highly dedicated online museum visitors</a>. <em>Information Research, 18</em>(4). Retrieved from http://www.informationr.net/ir/18-4/paper597.html#.UzPx6IVdBns
              </li>
              <li>Sub-Committee on Descriptive Standards. (1999). <em>ISAD (G): General international standard archival description</em> (2nd ed.). Stockholm, Sweden: International Council of Archives.
              </li>
              <li>Wilson, T. D. (2005). Evolution in information behavior modeling: Wilson's model. In K. E. Fisher, S. Erdelez &amp; L. E. F. McKechnie (Eds.), <em>Theories of information behavior</em> (pp. 31-36). New Jersey: Information Today Inc.
              </li>
              <li>Yin, R. K. (2009). How to do better case studies (with illustrations from 20 exemplary case studies). In L. Bickman &amp; D. J. Rog (Eds.), <em>The SAGE handbook of applied social research methods</em> (2nd ed., pp. 254-282). London: Sage Publications, Inc.
              </li>
            </ul>
          </fieldset>
        </form>
      </section>
    </article><br>
    <section>
      
      <br>
      <div style="text-align:center;">
        
        
      </div>
      <hr>
      
    </section>
    <footer>
      <hr>
      
      <hr>
    </footer>
  </body>
</html>
