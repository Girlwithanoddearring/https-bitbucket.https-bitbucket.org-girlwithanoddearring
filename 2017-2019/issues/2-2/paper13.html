<!DOCTYPE html>
<html>
<head>
<title>Stemming and N-gram matching for term conflation in Turkish texts</title>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<meta name="keywords" content="free text,  indexing,  retrieval, information retrieval, word forms, spelling errors, alternative spellings, multi-word concepts, transliteration, affixes, abbreviations, conflation algorithm, Turkish">
<meta name="description" content="One of the main problems involved in the use of free text for indexing and retrieval is the variation in word forms that is likely to be encountered.   The most common type of variations are spelling errors, alternative spellings, multi-word concepts, transliteration, affixes and abbreviations.  One way to alleviate this problem is to use a conflation algorithm, a computational procedure that is designed to bring together words that are semantically related, and to reduce them to a single form for retrieval purposes.  In this paper, we discuss the use of conflation techniques for Turkish text databases.">
<meta name="VW96.objecttype" content="Document">
<meta name="ROBOTS" content="ALL">
<meta name="DC.Title" content="Stemming and N-gram matching for term conflation in Turkish texts">
<meta name="DC.Creator" content="F. Cuna Ekmekcioglu, Michael F. Lynch, and Peter Willett">
<meta name="DC.Subject" content="free text,  indexing,  retrieval, information retrieval, word forms, spelling errors, alternative spellings, multi-word concepts, transliteration, affixes, abbreviations, conflation algorithm, Turkish">
<meta name="DC.Description" content="One of the main problems involved in the use of free text for indexing and retrieval is the variation in word forms that is likely to be encountered.   The most common type of variations are spelling errors, alternative spellings, multi-word concepts, transliteration, affixes and abbreviations.  One way to alleviate this problem is to use a conflation algorithm, a computational procedure that is designed to bring together words that are semantically related, and to reduce them to a single form for retrieval purposes.  In this paper, we discuss the use of conflation techniques for Turkish text databases.">
<meta name="DC.Publisher" content="Professor T.D. Wilson">
<meta name="DC.Coverage.PlaceName" content="Global">
<link rel="stylesheet" href="style.css">
</head>
<body>
<h4 id="information-research-vol-2-no-2-october-1996">Information Research, Vol. 2 No. 2, October 1996</h4>
<hr>
<h1 id="stemming-and-n-gram-matching-for-term-conflation-in-turkish-texts"><strong>Stemming and <em>N</em>-gram matching for term conflation in Turkish texts</strong></h1>
<h4 id="f-%C3%A7una-ekmek%C3%A7ioglu-michael-f-lynch-and-peter-willett">F. Çuna Ekmekçioglu, Michael F. Lynch, and <a href="mailto:p.willett@shef.ac.uk">Peter Willett</a></h4>
<p>Department of Information Studies<br>
University of Sheffield, Sheffield, UK</p>
<hr>
<h2 id="introduction">Introduction</h2>
<p>One of the main problems involved in the use of free text for indexing and retrieval is the variation in word forms that is likely to be encountered (<a href="#lenn81">Lennon, et al., 1981</a>) The most common type of variations are spelling errors, alternative spellings, multi-word concepts, transliteration, affixes and abbreviations. One way to alleviate this problem is to use a conflation algorithm, a computational procedure that is designed to bring together words that are semantically related, and to reduce them to a single form for retrieval purposes. In this paper, we discuss the use of conflation techniques for Turkish text databases.</p>
<p>Turkish is a member of the south-western or Oghuz group of the Turkic languages, which also includes Turkmen, Azerbaijani or Azeri, Ghasghai and Gagauz (<a href="#crys87">Crystal, 1987</a>; <a href="#lewi91">Lewis, 1991</a>). The Turkish language uses a Latin alphabet consisting of twenty-nine letters, of which eight are vowels and twenty-one are consonants. Unlike the main Indo-European languages (such as French and German), Turkish is an example of an <em>agglutinative language</em>, where words are a combination of several morphemes and suffixes. Here, words contain a root and suffixes are combined with this root in order to extend its meaning or to create other classes of words. In Turkish the process of adding one suffix to another can result in relatively long words, which often contain an amount of semantic information equivalent to a whole English phrase, clause or sentence. Due to this complex morphological structure, a single Turkish word can give rise to a very large number of variants, with a consequent need for effective conflation procedures if high recall is to be achieved in searches of Turkish text databases. Here, we summarise the principal results thus far of a project to develop and to evaluate such procedures; full details of the work are presented by <a href="#ekme95">Ekmekçioglu <em>et al.</em> (1995</a>, <a href="#ekme96">1996</a>).</p>
<h2 id="conflation-techniques">Conflation techniques</h2>
<p>Conflation algorithms can be broadly divided into two main classes: <em>stemming algorithms</em>, which are language dependent and which are designed to handle morphological variants, and <em>string-similarity algorithms</em>, which are (usually) language independent and which are designed to handle all types of variant.</p>
<h3 id="stemming-algorithms">Stemming algorithms</h3>
<p><a href="#lovi68">Lovins (1968)</a> defines a stemming algorithm as &quot;a procedure to reduce all words with the same stem to a common form, usually by stripping each word of its derivational and inflectional suffixes&quot;. Stemming is generally effected by means of suffix dictionaries that contain lists of possible word endings, and this approach has been applied successfully to many different languages. It is, however, less applicable to an agglutinative language such as Turkish, which require a more detailed level of morphological analysis, where purely morphological techniques are required that remove suffixes from words according to their internal structure. They proceed from the left to the right of a word and first look in a lexicon for a root matching an initial substring of the word; they then use grammatical information stored in the lexical entry to determine what possible suffixes may follow. When a suffix matching a further substring is found subsequent grammatical information in the lexical entry for that suffix again determines what class of suffixes may follow. The stemming is successful if the end of the word can be reached by iteration of this process, and if the last suffix is one which may end a word. Examples of agglutinative languages for which morphological analysers have been developed include Finnish (<a href="#kosk83">Koskenniemi, 1983</a>, <a href="#japp85">J�ppinnen, <em>et al.</em>, 1985</a>) and Turkish (<a href="#hank84">Hankamer, 1984;</a> <a href="#sola93">Solak. &amp; Oflazer, 1993</a>; <a href="#ofla94">Oflazer, 1994</a>).</p>
<h3 id="n-grams"><em>N</em>-Grams</h3>
<p>String-similarity approaches to conflation involve the system calculating a measure of similarity between an input query term and each of the distinct terms in the database. Those database terms that have a high similarity to a query term are then displayed to the user for possible inclusion in the query. <em>N-gram matching</em> techniques are one of the most common of these approaches (<a href="#freu82">Freund &amp; Willett, 1982</a>). An <em>n</em>-gram is a set of <em>n</em> consecutive characters extracted from a word. The main idea behind this approach is that, similar words will have a high proportion of <em>n</em>-grams in common. Typical values for <em>n</em> are 2 or 3, these corresponding to the use of digrams or trigrams, respectively. For example, the word BILGISAYAR(computer) results in the generation of the digrams</p>
<p><em>B, BI, IL, LG, GI, IS, SA, AY, YA, AR, R</em></p>
<p>and the trigrams</p>
<p><strong>B, <em>BI, BIL, ILG, LGI, GIS, ISA, SAY, AYA, YAR, AR</em>, R</strong></p>
<p>where '*' denotes a padding space. There are <em>n</em>+1 such digrams and <em>n</em>+2 such trigrams in a word containing <em>n</em> characters.</p>
<h2 id="experimental-details-and-results">Experimental details and results</h2>
<h3 id="stopword-list-and-compression-experiments">Stopword list and compression experiments</h3>
<p>The two main objectives to be achieved in the first stage of the research were the development of a stopword list for Turkish and the evaluation of a stemming algorithm that takes account of the language's morphological structure. The frequencies of occurrence of the word types in the Turkish corpora were found to follow a typical Zipfian distribution, with a few word types providing a very high percentage of the observed tokens. The most frequently occurring words were mainly function words such as conjunctions, postpositions, pronouns, <em>etc</em>., and, these words were selected for inclusion in the stopword list. Furthermore, some of the large numbers of low-frequency Turkish words were morphological variants of very commonly occurring function words; these former words were also included in the stopword list.</p>
<p>The experiments used a a political news database that contained 376,187 Turkish word tokens; experiments with additional corpora are reported by <a href="#ekme95">Ekmekçioglu <em>et al</em>. (1995)</a>. A dictionary was created from this database, and the stopwords eliminated. The two-level morphological parser, PC-KIMMO (<a href="#anth90">Anthworth, 1990</a>) and the PC-KIMMO description of the Turkish</p>
<hr>
<pre width="132">Corpus      Word       Word       Distinct  Distinct  Compression   
            Tokens     Types      Terms     Stems     (%)           

Turkish     376,187    49,479     41,370      6,363         84.6    

English     567,574    19,044     18,348    11,671          36.4    
</pre>
<hr>
<p><strong>Table 1</strong>. Level of compression in the six Turkish text corpora and in the English text.</p>
<p>language developed at Bilkent University [11] were employed to parse the words in the Turkish dictionaries to obtain the stems. The results of this procedure were evaluated in terms of percentage of compression, <em>i.e.</em>, in terms of the number of reduced words in the dictionary after stemming. For comparison, the results were compared with those obtained when <a href="#port80">Porter's (1980)</a> stemming algorithm for English was applied to a newswire database containing 567,574 English word tokens.</p>
<p>An inspection of Table 1 shows that the English database yields a much smaller number of word types than does the Turkish database, despite the fact that the former contains many more word tokens. This indicates the much greater richness of the variations that occur in the Turkish language, a conclusion that is reinforced by consideration of the compression results. Employment of the Turkish stemming algorithm reduces the number of the words by no less than 84.6%, which indicates that stemming can bring about very substantial reductions in the numbers of word variants that must be processed in a Turkish free-text retrieval system. The compression figure for the English database is far smaller, at 36.4%, illustrating clearly the lower degree of morphological variation in English.</p>
<h3 id="searching-experiments">Searching experiments</h3>
<p>A dictionary was created containing all of the word types, and each word was then represented by its lists of constituent digrams or trigrams. A query term was represented by an analogous list and then matched against each of the <em>n</em>-gram lists in turn to find the similarity between the query word and the corresponding dictionary word, using the overlap similarity measure. The dictionary words were ranked in decreasing order of similarity and some fixed number, 10 in the experiments reported here, of the top-ranked words displayed as the possible variants of the search term.</p>
<p>The query words here were a random selection of 50 words from the dictionary, and the effectiveness of a search was measured by the mean number of relevant words retrieved when averaged over the entire set of query words, where a relevant word is taken to be one that appears to be a correct variant of the chosen query word. Conventionally, <em>n</em>-gram matching has been applied to unstemmed document and query words. However, we have also carried out experiments that involve both stemming and <em>n</em>-gram matching. Specifically, each of the query words was stemmed prior to the implementation of the <em>n</em>-gram procedures. The experiments thus involve unstemmed documents and queries (the simple <em>n</em>-gram runs), and unstemmed documents and stemmed queries (the combined experiments).</p>
<p>The results of the <em>n</em>-gram searches are shown in Table 2, which suggests that the trigrams perform better, irrespective of whether or not the query words are stemmed prior to the</p>
<hr>
<pre width="132">_n_-grams         Unstemmed         Stemmed      
                Query Words    Query Words     

Digrams              4.76            5.80      

Trigrams             5.42            6.08      
</pre>
<hr>
<p><strong>Table 2</strong>. Mean number of relevant words (averaged over the entire set of query words) retrieved in the <em>n</em>-gram searching experiments with a cutoff of the top-10 words.</p>
<hr>
<pre width="132">                  Type-A    Type-B     
Stem search       8.56        7.33     
Combined search   8.48        6.08     
</pre>
<hr>
<p><strong>Table3</strong>. Mean number of relevant words (averaged over the entire set of query words) using the stem search and the combined search (trigrams and stemming of query words).</p>
<p>generation of the _n-_gram lists. The Sign test was used to determine the significance of the difference in performance between pairs of different sets of different searches. The trigram searches are significantly better(<em>p</em>=0.00) than the digram searches when no stemming is applied but there is no significant difference when stemming is applied to the query words prior to the generation of the <em>n</em>-gram lists. However, these combined searches are significantly better (<em>p</em>=0.00 for both digrams and trigrams) than the searches that use unstemmed query words. We thus conclude that the best results are obtained by using trigrams derived from the stemmed query terms.</p>
<p>Further tests were carried out in which the best <em>n</em>-gram searches, <em>i.e.</em>, those involving both trigrams and stemming, were compared with searches in which both the query words and the dictionary words were stemmed, which is the most common way of implementing stem-based retrieval. Such a comparison requires a way of equalising the outputs of the two types of search, and two approaches were taken to achieve this [5]. In both cases, a stem search is carried out for a query word and the number, <em>s</em>, of matching stems noted. In the first approach, referred to as Type-A searches, the corresponding <em>n</em>-gram search was evaluated using the top ranked <em>s</em> words. In the second, Type-B, approach, if <em>s</em>&lt;10 then 10-<em>s</em> non-relevant words were added to the <em>s</em> words that were retrieved; otherwise the number of relevant words was multiplied by 10/<em>s</em>. The Type-A searches are thus biased towards the stem searches while the Type-B searches are biased towards the <em>n</em>-gram searches.</p>
<p>The results of this comparison is shown in Table 3, where it will be seen that stemming is superior (but not significantly so; <em>p</em>=0.26 and <em>p</em>=0.07, respectively) to the combined search in both the Type-A and Type-B searches</p>
<h2 id="conclusions">Conclusions</h2>
<p>Our experiments show that the use of a stopword list and a stemming algorithm can bring about substantial reductions in the numbers of word variants encountered in searches of Turkish text databases; moreover, stemming appears to be superior, but not significantly so, to <em>n</em>-gram matching for conflating such variants. However, the experiments thus far have been limited in that have involved only single-word searches of a dictionary. The procedures are thus now being implemented in a version of the OKAPI (<a href="#walk88">Walker, 1988</a>) retrieval system to carry out both conflated and non-conflated searches of Turkish databases. This work will be reported shortly.</p>
<h2 id="acknowledgements">Acknowledgements</h2>
<p>We thank Dr. Sandy Robertson and Dr. Kemal Oflazer for assistance and helpful discussions, and Dr. Hilmi Çelik, ulector of the Library of the Turkish Parliament, for the provision of the political news database used in this study.</p>
<h2 id="references">References</h2>
<ul>
<li><a name="anth90">Anthworth, E.L</a>. (1990) <em>PC-KIMMO: A Two-level Processor for Morphological Analysis</em>. Dallas, Texas: Summer Institute of Linguistics.</li>
<li><a name="crys87">Crystal, D</a>. (1987)<em>The Cambridge Encyclopaedia of Language</em>. Cambridge: Cambridge University Press.</li>
<li><a name="ekme95">Ekmekçioglu, F</a>.Ç., Lynch, M. F. &amp; Willett, P. (1995) Development and evaluation of conflation techniques for the implementation of a document retrieval system for Turkish text databases. <em>The New Review of Document and Text Management</em>, <strong>1</strong>, 131-146.</li>
<li><a name="ekme96">Ekmekçioglu, F.</a>Ç., Lynch, M.F., Robertson, A.M., Sembok, T.M.T. &amp; Willett, P. (1996) Comparison of <em>n</em>-gram matching and stemming for term conflation in English, Malay, and Turkish texts. <em>Text Technology</em>, <strong>6</strong>, 1-14.</li>
<li><a name="freu82">Freund, G.E.</a> &amp; Willett, P. (1982) Online identification of word variants and arbitrary truncation searching using a string similarity measure. <em>Information Technology: Research and Development</em>, <strong>1</strong>, 177-187.</li>
<li><a name="hank84">Hankamer, J.</a> (1984) Turkish generative morphology and morphological parsing. Presented at the <em>Second International Conference on Turkish Linguistics</em>, Istanbul.</li>
<li><a name="japp85">Jäppinnen, H.,</a> Niemisto, J. &amp; Ylilammi, M. (1985) FINNTEXT - text retrieval system for an agglutinative language. <em>RIAO 85 Recherche d'Informations</em>, Grenoble: IMAG, 1985, 217-226</li>
<li><a name="kosk83">Koskenniemi, K.</a> (1983) <em>Two-level Morphology: A General Computational Model for Word-Form Recognition and Production</em>. Publications of the Department of General Linguistics, 11. Helsinki: University of Helsinki.</li>
<li><a name="lenn81">Lennon, M.,</a> Peirce, D.S., Tarry, B.D. &amp; Willett, P. (1981) An evaluation of some conflation algorithms for information retrieval. <em>Journal of Information Science</em>, <strong>3</strong>, 177-183.</li>
<li><a name="lewi91">Lewis, G.L.</a> (1991) <em>Turkish Grammar.</em> Oxford: Oxford University Press, 1991.</li>
<li><a name="lovi68">Lovins, J.B.</a> (1968) Development of a stemming algorithm. <em>Mechanical Translation and Computational Linguistics</em>, <strong>11</strong>, 1968, 22-31.</li>
<li><a name="ofla94">Oflazer, K.</a> (1994) Two-level description of Turkish morphology. <em>Literary and Linguistic Computing</em>,<strong>9</strong>, 175-198.</li>
<li><a name="port80">Porter, M.F.</a> (1980) An algorithm for suffix stripping. <em>Program</em>, <strong>14</strong>, 130-137.</li>
<li><a name="sola93">Solak, A.</a> &amp; Oflazer, K. (1993) Design and implementation of a spelling checker for Turkish. <em>Linguistic and Literary Computing,</em> <strong>8</strong>, 1993.</li>
<li><a name="walk88">Walker, S.</a> (1988) Improving subject access painlessly: recent work on the Okapi online catalogue projects. <em>Program</em>, <strong>22</strong>, 21-31.</li>
</ul>
<hr>

</body>
</html>
