---
title: "The impact of information on clinical decision making by General Medical Practitioners"
---
#### Information Research, Vol. 2 No. 1, August 1996

<a name="paper11"></a>

# The impact of information on clinical decision making by General Medical Practitioners

#### Frances Wood and Pamela Wright  
Department of Information Studies  
University of Sheffield, Sheffield, UK

## Introduction

This paper summarises some of the principal findings of a recent study investigation of information usage by general medical practitioners (GPs) ([Wood et al., 1995a](#wood2)). The work was based on previous studies of the value and impact of information, these studies being undertaken in the corporate sector in Canada ([Marshall, 1993](#mars2)) and in the health care sector in both the USA ([King, 1987](#king); [Marshall, 1992](#mars1)) and the UK ([Hepworth and Urquhart, 1995](#hepw); Wood et al., 1995b, 1996). The study used a critical incident technique similar to that employed in the Canadian and USA studies. Twenty seven in-depth interviews were conducted with general practitioners (GPs) in the Trent Health Region (only one from each practice). The sample, selected from two health districts, included large, medium and small practices, fund-holding and non-fund-holding practices, and training and non-training practices, with some representation of those located in deprived and non-deprived (socio-economic) areas.

The clinical study of [Marshall (1992)](#mars1) focused on hospital physicians who have ready access to their organisations? special libraries, which is not the case with general practitioners in the United Kingdom. It was known from previous studies [(Wood et al., 1995b)](#wood2) that GPs were likely to make little direct use of libraries in their patient-related decision-making so a much broader approach was taken. It was, however, a requirement of the project that the interview schedule incorporate certain of the questions included in earlier studies ([Marshall, 1992](#mars1), [1993](#mars2)), to allow comparisons to be made with them. These comparisons related specifically to three areas: the quality of the information; its cognitive value; and its value in the decision-making process. Details of GPs? general use of libraries were gathered separately.

## Characteristics of the participants and the critical incidents

Of the 120 practices contacted 38 expressed willingness to participate. Lack of time was the most common reason given for non-participation, although some GPs stated that they had decided not to participate in further studies, and several others would have taken part had a financial incentive been offered. The participants varied greatly in the length of their professional experience: seven of them had been in practice for less than 5 years; four for 5-10 years; seven for 11-15 years; and the remaining nine for between 16 and 30 years.

The participants were required to state the last time they had sought information to assist in the decision-making process related specifically to a patient care problem. In fourteen of the cases, the information had been required less than one week before; in eight it had been required one to two weeks before, and in one it had been required upto one month before. The remaining four GPs based their responses on information sought more than one month previously, but it is likely that they had misunderstood what was meant by ?information?, since: one stated during the course of the interview that she had telephoned a Regional Medical Officer the previous day; one reads extensively and is involved in the area?s Vocational Training Scheme; one rang an Orthopaedist the afternoon of the interview for advice about a patient; and the last uses MIMS ((Monthly Index of Medical Specialities) "very regularly". Several GPs commented that they needed information most frequently about drugs, with MIMS, the ABPI Data Compendium or the BNF (British National Formulary) being consulted. These were generally considered to be excellent, regularly-updated sources but there is no single, standard reference source for drug information.

GPs were asked to give details of the patient problem presented and the specific types of information sought, as detailed in Table 1\. When the interviews were analysed it was found that in seven cases, although the information was important for patient care, it was not related to clinical decisions but was of an administrative nature. These cases are discussed later in the paper.

<table align="center" bgcolor="#FDFFBB" border="1" cellpadding="6">

<tbody>

<tr>

<th>**Type of Information Sought**</th>

<th>**Medical**</th>

<th>**Administrative**</th>

</tr>

<tr>

<td>Details of hospital referral system</td>

<td align="center">2</td>

<td align="center">5</td>

</tr>

<tr>

<td>Drug related enquiries</td>

<td align="center">5</td>

<td align="center">1</td>

</tr>

<tr>

<td>Medical opinion of colleagues</td>

<td align="center">6</td>

<td> </td>

</tr>

<tr>

<td>Results of laboratory tests</td>

<td align="center">2</td>

<td> </td>

</tr>

<tr>

<td>Information from patients' hospital records</td>

<td align="center">2</td>

<td> </td>

</tr>

<tr>

<td>Results of X-ray tests</td>

<td align="center">1</td>

<td> </td>

</tr>

<tr>

<td>Confirmation of initial diagnosis from medical text</td>

<td align="center">1</td>

<td> </td>

</tr>

<tr>

<td>Evidence of protocol on management of illness</td>

<td align="center">1</td>

<td> </td>

</tr>

<tr>

<td>Social services protocol related to patient care</td>

<td> </td>

<td align="center">1</td>

</tr>

</tbody>

<caption align="bottom">  
**Table 1\. Types of information sought (n=27)**</caption></table>

The next table, Table 2, shows the sources used to find the information that was used in clinical decision-making. In summary, thirteen of the responses involved internal (reference books: 5; GPs? own reading: 4; and internal colleagues: 4) information sources and fourteen involved external (external colleagues: 9; laboratory tests or results: 2; hospital staff for medical records: 1; X-ray results: 1; and a training course: 1) information sources. No use was made of external library services to assist in these decision-making situations, although one GP did use the practice library to consult a medical text book.

<table align="center" bgcolor="#FDFFBB" border="1" cellpadding="6">

<tbody>

<tr>

<th><a name="table2"></a>**Information Sought**</th>

<th>**External Source**</th>

<th>**Internal Source(s)**</th>

</tr>

<tr>

<td>Protocol for urgent hospital referral</td>

<td>Hospital registrar</td>

<td> </td>

</tr>

<tr>

<td>Drug dosage</td>

<td> </td>

<td>BNF</td>

</tr>

<tr>

<td>Drug dosage and suitability</td>

<td> </td>

<td>ABPI - Data sheet compendium</td>

</tr>

<tr>

<td>Drug constituents</td>

<td> </td>

<td>MIMS</td>

</tr>

<tr>

<td>Drug side-effects</td>

<td> </td>

<td>Data sheet compendium</td>

</tr>

<tr>

<td>Drug suitability - literature references</td>

<td>Drug Information Service</td>

<td> </td>

</tr>

<tr>

<td>Patient's medical records - treatment</td>

<td>Hospital Registrar</td>

<td> </td>

</tr>

<tr>

<td>Patient's medical records - medication</td>

<td>Consultant's secretary (medical records)</td>

<td> </td>

</tr>

<tr>

<td>Results of laboratory test</td>

<td>Hospital staff</td>

<td> </td>

</tr>

<tr>

<td>Results of laboratory test</td>

<td>Pathology Unit staff</td>

<td> </td>

</tr>

<tr>

<td>Results of X-ray</td>

<td>X-ray Department staff</td>

<td> </td>

</tr>

<tr>

<td>Medical opinion</td>

<td> </td>

<td>Practice partner</td>

</tr>

<tr>

<td>Medical opinion</td>

<td>Biochemist</td>

<td>Practice partners</td>

</tr>

<tr>

<td>Medical opinion</td>

<td>Hospital Registrar</td>

<td>Practice partners; nursing staff; Sheffield Protocol on Osteoporosis</td>

</tr>

<tr>

<td>Medical opinion</td>

<td>Hospital Consultant</td>

<td> </td>

</tr>

<tr>

<td>Medical opinion</td>

<td>Hospice Consultant</td>

<td> </td>

</tr>

<tr>

<td>Medical opinion</td>

<td>Biochemist</td>

<td> </td>

</tr>

<tr>

<td>Printed confirmation of diagnosis</td>

<td> </td>

<td>Practice Library text</td>

</tr>

<tr>

<td>Guidelines on illness management</td>

<td>Meeting attended</td>

<td>Practice protocol on treatment of high cholesterol; Journal article; BMA guidelines</td>

</tr>

</tbody>

<caption align="bottom">  
**Table 2\. Sources of information used (n=20)**</caption></table>

### Assessments of the value of the information

The three main areas considered were: the quality of the information; the cognitive value of the information to the GP; and the value of the information in the decision-making situation. The results of the analysis are shown in Table 3.

<table align="center" bgcolor="#FDFFBB" border="1" cellpadding="6"><caption align="bottom">  
**Table 3: Value of the information in terms of numbers of positive responses (n=20)**  
[Note: Not every participant responded to every question, so the numbers of respondents are shown in parentheses.]</caption>

<tbody>

<tr>

<th colspan="2">Value of the information</th>

<th>Number</th>

</tr>

<tr>

<td rowspan="2">**Quality**</td>

<td>Relevant</td>

<td align="center">18 (19)</td>

</tr>

<tr>

<td>Accurate and current</td>

<td align="center">18 (18)</td>

</tr>

<tr>

<td rowspan="3">**Cognitive value**</td>

<td>Refreshed memory of detail or facts</td>

<td align="center">13 (19)</td>

</tr>

<tr>

<td>Substantiated prior knowledge or belief</td>

<td align="center">14 (19)</td>

</tr>

<tr>

<td>Provided new knowledge</td>

<td align="center">12 (19)</td>

</tr>

<tr>

<td rowspan="3">**Contribution to quality patient care**</td>

<td>Information was of medical value</td>

<td align="center">15 (18)</td>

</tr>

<tr>

<td>Better informed medical decisions</td>

<td align="center">17 (19)</td>

<td></td>

</tr>

<tr>

<td>Contributed to higher quality care</td>

<td align="center">18 (20)</td>

<td></td>

</tr>

<tr>

<td colspan="2">**Saved GP time**</td>

<td align="center">8 (19)</td>

</tr>

</tbody>

</table>

Earlier studies (Marshall, 1992, 1993) have investigated the impact of information provided by special libraries in the clinical and corporate settings. However, for reasons that have been discussed already, this study concentrated on the information sought by the GP himself (herself) from a pertinent source of his or her choosing.

All except one of the GPs were satisfied with the quality of the information, which was otherwise considered to be relevant, accurate and current. The one GP who was not satisfied with the relevancy of the information was unsure of the accuracy of the television program his patient had been watching, which had stimulated her enquiry. Approximately two thirds of the participants considered that the information had cognitive value in the terms used in this study. Almost all GPs agreed that the information did contribute to quality patient care; of those who were not satisfied one made the following remarks:

> "...they told me that there was absolutely nothing they could do or suggest, and nothing that I could do that would make things any better. So it was really a very mechanistic approach which was of no use to me whatsoever in managing my patient. Anything I suggested they said wouldn?t work, so it was extremely negative."

Fewer than half of the participants stated that the information saved time. In many cases the process of accessing the relevant information was time-consuming; once the information was to hand most of the GPs were able to make the appropriate decisions and some stated that it would save time in the future. The answers to this question were strongly influenced by the GPs? willingness, or otherwise, to delegate the information-seeking task; only two participants had delegated the task to practice staff, with both of these tasks being requests for the results of laboratory tests.

#### Impact on decision-making: was the situation handled differently?

These data represent the measurable impact of the information; the GP might have felt more confident having received the information but a more significant aspect is the extent to which the situation was handled differently. Of the 20 GPs, the responses were as follows: definitely yes: 12; probably yes: 2; probably no: 3; definitely no: 3\. The information thus had an influence on the GP?s handling of the situation in no less than 14 of the 20 cases; of these 14, four GPs said the changes were of some importance to the patient, six of considerable importance and four of great importance.

#### Impact of the information: changes in patient care.

The most important elements in an impact study are whether the information actually makes a difference to the decisions made and whether it allows the recipient to advance the process more effectively. The next table (Table 4), which is adapted from that used in the Rochester (USA) study (Marshall, 1992), details the changes reported by the 14 GPs who reported that the information had brought about a change in the handling of the patient. The results show the GPs were able to provide better patient care because of the information. Three GPs changed two aspects of patient care and seven changed one aspect of care.

<table align="center" bgcolor="#FDFFBB" border="1" cellpadding="6"><caption align="bottom">  
**Table 4: Changes in patient care (n=14) for the GPs who stated that the informationhad resulted in a change in the way that the situation was handled.**  
Some participants reported more than one change in patient care.</caption>

<tbody>

<tr>

<th>Way In Which The Situation Was Handled Differently</th>

<th>Number</th>

</tr>

<tr>

<td>Choice of tests</td>

<td align="center">3</td>

</tr>

<tr>

<td>Choice of drugs</td>

<td align="center">3</td>

</tr>

<tr>

<td>Post-hospital care or treatment</td>

<td align="center">3</td>

</tr>

<tr>

<td>Changed advice given to patient</td>

<td align="center">3</td>

</tr>

<tr>

<td>Patient management decision</td>

<td align="center">3</td>

</tr>

<tr>

<td>Hospital admission</td>

<td align="center">3</td>

</tr>

</tbody>

</table>

#### Impact of the information: avoidance of negative effects on patient care

Table 5, which was adapted from the clinical study by [Marshall (1992)](#mars1), illustrates both the negative and positive effects afforded by the use of the information. The table also shows that in several instances the information obtained enabled the GP to avoid having to ask the patient to undergo additional tests, referral or even admission to hospital. Even more significant is that in two cases the patient recovered when s/he might not have done and that in 11 instances the health status of the patient improved when it might not otherwise have done. Several GPs were keen to stress that the patient?s psychological state was just as important as their physical health and that the information gained had had a positive effect on this.

<table align="center" bgcolor="#FDFFBB" border="1" cellpadding="6"><caption align="bottom">  
**Table 5: Effect of the availability of information on patient care (n=14).**  
Some participants mentioned more than one change in patient care.</caption>

<tbody>

<tr>

<th>Effect of the Information</th>

<th>Number</th>

</tr>

<tr>

<td>Avoiding additional tests or other medical procedures</td>

<td align="center">7</td>

</tr>

<tr>

<td>Avoiding referral to a consultant</td>

<td align="center">6</td>

</tr>

<tr>

<td>Avoiding admission to hospital</td>

<td align="center">3</td>

</tr>

<tr>

<td>Patient recovered when they might not have done</td>

<td align="center">2</td>

</tr>

<tr>

<td>Health status improved when it might not have done</td>

<td align="center">11</td>

</tr>

</tbody>

</table>

#### Importance of the availability of different information sources

[Table 2](#table2) illustrates the variety of information sources used by the GPs in the decision-making situation. Seventeen used one source only to satisfy the information need, one used two different sources and two GPs used four sources. Colleagues, both within and outside of the Practice, were the most popular sources of information. The most frequently used sources of ?internal? information were the BNF, ABPI and MIMS that offer drug-related information.

In this impact study none of the respondents had sought information from an external medical library but one GP had referred to his practice library. The general use of libraries by GPs was investigated and this is considered further below.

#### The Impact Of Non-Medical Information

Seven of the critical incidents described proved to be concerned with seeking administrative information and did not involve any clinical decision-making. Of these, five incidents required details of hospital referral systems (four of them for urgent referrals) from hospital staff, one required the availability of a particular drug at a particular pharmacy from the pharmacist, and one required a Social Services protocol for admission to a home from the duty officer. Only one GP delegated the task of obtaining information for hospital referrals to the practice staff.

The major impact of the non-medical information was in its contribution to higher quality care, with three GPs stating that the information was instrumental in enabling the patient to recover more quickly. This serves to illustrate the importance of, and necessity for, quality procedural information to accompany quality medical information (Table 6).

<table align="center" bgcolor="#FDFFBB" border="1" cellpadding="6"><caption align="bottom">  
**Table 6: Value of non-medical information**</caption>

<tbody>

<tr>

<th colspan="2">Value of non-medical information</th>

<th>Number of Positive Responses</th>

</tr>

<tr>

<td rowspan="2">**Quality**</td>

<td>Relevant</td>

<td align="center">5 (5)</td>

</tr>

<tr>

<td>Accurate and current</td>

<td align="center">4 (5)</td>

</tr>

<tr>

<td rowspan="3">**Cognitive value**</td>

<td>Refreshed memory of detail or facts</td>

<td align="center">2 (5)</td>

</tr>

<tr>

<td>Substantiated prior knowledge or belief</td>

<td align="center">3 (5)</td>

</tr>

<tr>

<td>Provided new knowledge</td>

<td align="center">3 (5)</td>

</tr>

<tr>

<td rowspan="3">**Contribution to quality patient care**</td>

<td>Information was of value to patient care</td>

<td align="center">2 (5)</td>

</tr>

<tr>

<td>Better informed decisions</td>

<td align="center">1 (5)</td>

<td></td>

</tr>

<tr>

<td>Contributed to higher quality care</td>

<td align="center">4 (5)</td>

<td></td>

</tr>

</tbody>

</table>

In considering whether the situation was handled differently, the majority opinion was in the negative and as such it probably reflected the fact that the medical decision had already been made. The three GPs who had handled (or would handle) the situation differently each stated that the changes were of considerable importance for the patient, e.g.: "_it brought his appointment forward by ten, eleven weeks_"

Ready access to such non-medical information is of vital importance to the GP?s management of the case.

### GPs? use of external library sources

One of the participants remarked that: "general practitioners hardly ever visit libraries, they don?t get lots of other publications except maybe the BMJ, and other journals because they?re free"

reflecting the findings of previous studies ([Childs, 1988](#chil); [Elayyan, 1988](#elay)). GPs do not use a library for immediate patient care problems but the majority (26 of the 27 participants) stated that they read the British Medical Journal (BMJ). It would appear that the library is considered to be a collection of books and journals to be accessed when the GP has time, rather than as an information service.

All GPs in this study were asked to state the last time that they had used an external library in connection with their work. Of the twelve GPs who had used an external library within the previous six months: seven had used hospital medical libraries; three the Post-graduate Centres attached to hospitals; one the Health Authority library; and one the RCGP library. The librarian was involved in obtaining information in six cases, one of which was to arrange a CD-ROM training session and two were related to telephone requests. Nine GPs visited the library, two telephoned, and one sent a postal enquiry. The GPs sought: CD-ROM training; literature searches for new titles; and medical literature searches related to a non-urgent patient case, related to litigation, in preparation for a talk, for writing a paper, and for researching a project.

None of the examples of library use was directly related to an immediate patient problem; some were in connection with the treatment of specific illnesses, some with continuing education and some with research. Contact with the library was rated as ?Very successful? by six GPs and ?Successful? by a further six GPs. An additional three GPs had used library services within the previous twelve months, one when he was studying for an examination, one for the purposes of post-graduate education, and one in connection with an audit he was conducting. The availability of an efficient, effective external library service is obviously highly valued by some GPs. "_One of the best things for me is having contact with libraries._"

One interesting comment from a participant was:

> "I only use it (the library) occasionally, so...I don?t know my way around...and I?m conscious of (a) not appearing an idiot and (b) taking up...the librarian?s time...It would be...good to ring somebody up in the library and say "Can you dig me out a few bits and pieces on.."...you?d be really grateful and possibly willing to pay for it"

Several of the participants who had not used an external library in connection with their work during the previous six months were avid readers of journals, had outside interests and were keen on information technology in medicine. Physical access to the nearest medical library, especially parking, was regarded as a major problem by several GPs.

Seventeen of the GPs were members of the BMA; of these, only one mentioned using the BMA library and now prefers to use the CD-ROM version of MEDLINE. Eighteen were members of the RCGP but only one GP mentioned using their library recently and one stated that membership was "not that useful". When speaking about their reading habits GPs used terminology such as ?indulgence?, ?privileges?, and reading and discussing in ?protected time?. This choice of language may reflect the view that the opportunity for reading is regarded as a luxury, rather than an integral part of a GP?s working day.

## Discussion

In this study the GPs were asked to describe the last time they had needed to seek information to help with the care of a patient. Almost all could describe an event within the last week, e.g., finding out the arrangements for a particular referral, checking on the most suitable drug for a child, reviewing a case, checking if a patient had to come off particular medication prior to an operation. Sometimes the information was needed whilst the patient was with the GP. The information was obtained mainly by telephoning hospital staff, including consultants, but other sources included MIMS, drug compendia, the practice library, meetings and colleagues. In about two-thirds of the cases the information obtained changed the way the situation was handled. In about one-third of cases, the information obtained changed the choice of tests, drugs, post-hospital care, or advice given to the patient.

In a few cases the actual diagnosis was changed because of the information obtained. In some cases GPs also reported that the information they obtained meant that they did not send the patient for additional tests or other procedures, refer the patient to a consultant, or arrange hospital admission. Over half the GPs said that the health status of the patient had improved when it otherwise might not have done. A few of the GPs said that the patient recovered when s/he might not have done or that the patient recovered more quickly (this was related to non-medical information).

Information is often needed immediately for individual patient care. Already some GP computer systems include a drug compendium which contains information on adverse reactions and contra- indications. Computer-based information systems, such as Personalised Medical Reference ([McMorran et al., 1995](#mcmo)) which aims to encapsulate essential medical knowledge and the Oxford Handbook of Medicine, will become more generally available but the extent to which the contents of these are research based will vary. Some GPs may choose also to have MEDLINE or other databases on CD-ROM in the practice or to access MEDLINE via the BMA but will still need the services of librarians to enable them to learn effective searching strategies and to obtain documents.

For some of the critical incidents, evidence-based or research-based information was not needed but for others evidence of medical, drug, or care provider performance was required. It appears, however, that few of the GPs directly used such information although the consultants, and other hospital staff consulted by the GPs, could have been basing their advice on published research or other evidence. For this reason additional questions were asked about the GPs? use of information sources. When asked to consider the importance of sources of information over half the GPs considered colleagues in hospitals important. Other GPs considered reference books, their own colleagues, reading and courses/meetings as important. The only journal which was read by almost all the GPs was the BMJ. The only other publications most of the GPs said they saw or read were books, free publications and drug company leaflets. A few had carried out a MEDLINE search or had one done for them. Practice libraries, where they existed, appeared to be well used, especially for looking at journals.

As these results show, human sources were considered to be more important than literary sources in the context of immediate patient care problems. In the six months prior to the study less than half of the GPs had used a medical library, other than their practice library, and seven had not used an external library for many years. Physical access to those libraries attached to hospitals was a problem in several cases; this may be improved with technological changes, although some GPs were concerned about the lack of computer capacity to avail themselves of such services. GPs do use libraries for research, continuing education and non-immediate reasons, but there seems to be a lack of awareness of exactly what services are available, and in some cases a lack of knowledge of how to use those services. The service offered by the Sheffield Health Authority in circulating the details of new publications is commendable, but only one of the participants in this study availed himself of it. More GPs are entitled to access the libraries and information services of the BMA and RCGP than use them.

## Acknowledgements

This project was funded by the British Library Research and Development Department. The willing participation of so many busy GPs is gratefully acknowledged, as are the contributions of the Project Head, Professor [Tom Wilson](mailto:t.d.wilson@shef.ac.uk).

## References

*   <a name="chil">Childs, S.M.</a> (1988). General practitioners? use of NHS libraries. Health Libraries Review, 5(2), 76-92.
*   <a name="elay">Elayyan, M.</a> (1988). The use of information by physicians. International Library Review, 20, 247-265.
*   <a name="hepw">Hepworth, J. B. and Urquhart, C. J.</a> (1995). The value of databases in clinical decision making: audit tools to enhance the outcomes of information delivery. In: Richards, B. (editor) Current Perspectives in Healthcare Computing. Weybridge: BJHC (Healthcare Computing Conference 1995, Harrogate 20-22 March) pp. 45-51.
*   <a name="king">King, D.</a> (1987). The contribution of hospital library services to patient care. Bulletin of the Medical Library Association, 75, 291-301.
*   <a name="mars1">Marshall, J.G.</a> (1992). The impact of the hospital library on clinical decision-making: the Rochester study. Bulletin of the Medical Library Association, 80, 69-178.
*   <a name="mars2">Marshall, J.G.</a> (1993). The Impact of the Special Library on Corporate Decision-Making. Washington DC: Special Libraries Association.
*   <a name="mcmo">McMorran, J.P.,</a> McMorran, S.H., Wacogne, I., Crowther, D., Pleat, J., Young-Min, S, and Rowbotham, C. (1995). Personalised medical reference: a tool for the practitioner. In: Richards, B. (editor) Current Perspectives in Healthcare Computing. Weybridge: BJHC (Healthcare Computing Conference 1995, Harrogate 20-22 March) pp. 455-461.
*   <a name="wood2">Wood, F. E.,</a> Wright, P. and Wilson, T. D. (1995a) The Impact of Information Use on Decision Making by General Medical practitioners. London: British Library (British Library R&D Report number awaited).
*   <a name="wood1">Wood, F.E.,</a> Ellis, D., Bacigalupo, R. and Simpson, S. (1995b). Information in General Medical Practice: A Qualitative Approach. London: British Library (British Library R&D Report 6191).
*   <a name="wood3">Wood, F.E.,</a> Palmer, J., Bacigalupo, R., Simpson, S. and Wright, P. (1996). General practitioners and information: evidence based practice ex plored. Paper presented at the Healthcare Computing Conference, Harrogate, March 1996.